package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        String str = "ániTa,lava.la tina";
        System.out.println("El String: " + str);
        String irr = quitarIrregularidades(str);
        System.out.println("Sin Irregularidades: "+ irr);
        boolean palin = esPalindromo(irr);
        //System.out.println(palin);
        if(palin = true)
            System.out.println("Es Palindromo");
        else
            System.out.println("NO es Palindromo");
    }

    public static String quitarIrregularidades(String str){
        //Quitar Espacios en blanco
        String esp = str.replaceAll("\\s+","");
        //Quitar puntos
        esp= esp.replaceAll("\\.+","");
        //Quitar comas
        esp= esp.replaceAll("\\,+","");
        //Quitar Tildes de vocales
        esp= esp= esp.replaceAll("\\á+","a");
        esp= esp= esp.replaceAll("\\é+","e");
        esp= esp= esp.replaceAll("\\í+","i");
        esp= esp= esp.replaceAll("\\ó+","o");
        esp= esp= esp.replaceAll("\\ú+","u");
        //Todo se cambia a minusculas
        esp = esp.toLowerCase();

        return esp;
    }


    public static boolean esPalindromo(String str){
        int i1 = 0;
        int i2 = str.length() - 1;
        while (i2 > i1) {
            if (str.charAt(i1) != str.charAt(i2)) {
                return false;
            }
            ++i1;
            --i2;
        }
        return true;
    }
}
